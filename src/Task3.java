import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Task3 {

	public static Map<String, Integer> doTask(List<MonitoredData> data) {

		Map<String, Integer> activityCount = null;
		activityCount = data.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(e -> 1)));
		return activityCount;
	}

	public static void writeTask(Map<String, Integer> activityCount) {

		File task3 = new File("Task_3.txt");
		try {
			FileWriter fw3 = new FileWriter(task3);
			fw3.write("Occurences of each activity: \n\n");
			for (Entry<String, Integer> entry : activityCount.entrySet())
				fw3.write(entry.getKey() + ": " + entry.getValue() + "\n");
			fw3.flush();
			fw3.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
