import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {

	private String startTime;
	private String endTime;
	private String activityLabel;

	public MonitoredData(String start_time, String end_time, String activity_label) {
		super();
		this.startTime = start_time;
		this.endTime = end_time;
		this.activityLabel = activity_label;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String start_time) {
		this.startTime = start_time;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String end_time) {
		this.endTime = end_time;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activity_label) {
		this.activityLabel = activity_label;
	}

	public String getDay() {
		return startTime.substring(0, 10);
	}

	public int getDayInt() {

		return Integer.parseInt(startTime.substring(5, 7) + startTime.substring(8, 10));
	}

	public Duration getDuration() {

		Duration d = null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime start = LocalDateTime.parse(startTime, formatter);
		LocalDateTime end = LocalDateTime.parse(endTime, formatter);
		d = Duration.between(start, end);
		return d;
	}

	public boolean isShortActivity() {

		if (getDuration().getSeconds() < 300)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "Start: " + startTime + "	End: " + endTime + "	Label: " + activityLabel + "\n";
	}

}
