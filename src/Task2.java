import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Task2 {

	public static List<String> doTask(List<MonitoredData> data) {

		List<String> days = null;
		days = (List<String>) data.stream().map(MonitoredData::getDay).collect(Collectors.toList());
		return days;
	}

	public static void writeTask(List<String> days) {

		File task2 = new File("Task_2.txt");
		long distinctDays = days.stream().distinct().count();
		try {
			FileWriter fw2 = new FileWriter(task2);
			fw2.write("Distinct days: " + distinctDays + "\n");
			days.stream().distinct().forEach(e -> {
				try {
					fw2.write("\n" + e);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
			fw2.flush();
			fw2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
