import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class Task5 {

	public static Map<String, Duration> doTask(List<MonitoredData> data) {

		Map<String, Duration> duration = null;
		duration = data.stream()
				.collect(toMap(MonitoredData::getActivityLabel, MonitoredData::getDuration, Duration::plus));
		return duration;
	}

	public static void writeTask(Map<String, Duration> duration) {

		File task5 = new File("Task_5.txt");
		try {
			FileWriter fw5 = new FileWriter(task5);
			fw5.write("Total duration of each activity (HH:MM:SS):\n\n");
			for (Map.Entry<String, Duration> entry : duration.entrySet()) {
				fw5.write(entry.getKey() + ": ");
				int s = (int) entry.getValue().getSeconds();
				fw5.write(String.format("%d:%02d:%02d", s / 3600, (s % 3600) / 60, (s % 60)) + "\n");
			}
			fw5.flush();
			fw5.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
