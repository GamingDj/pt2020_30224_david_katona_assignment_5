import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Task4 {

	public static Map<Integer, Map<String, Integer>> doTask(List<MonitoredData> data) {

		Map<Integer, Map<String, Integer>> activityByDay = null;
		activityByDay = data.stream().collect(Collectors.groupingBy(e -> e.getDayInt(),
				Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(e -> 1))));
		return activityByDay;
	}

	public static void writeTask(Map<Integer, Map<String, Integer>> activityByDay) {

		File task4 = new File("Task_4.txt");
		try {
			FileWriter fw4 = new FileWriter(task4);
			fw4.write("Occurences of each activity by day: \n\n");
			for (Entry<Integer, Map<String, Integer>> entry : activityByDay.entrySet()) {
				String day = entry.getKey().toString();
				StringBuffer str = new StringBuffer(day);
				str.insert(2, "-");
				fw4.write("Day: " + str + "\n");
				for (Entry<String, Integer> entry2 : entry.getValue().entrySet()) {
					fw4.write(" -> " + entry2.getKey() + ": " + entry2.getValue() + "\n");
				}
				fw4.write("\n");
			}
			fw4.flush();
			fw4.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
