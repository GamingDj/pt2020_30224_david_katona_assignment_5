import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task6 {

	public static List<String> doTask(List<MonitoredData> data) {

		Map<String, List<MonitoredData>> allActivities = null;
		List<String> shortActivities = new ArrayList<String>();

		allActivities = data.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel));
		for (String activityName : allActivities.keySet()) {
			float totalSize = (float) allActivities.get(activityName).size();
			float matches = (float) allActivities.get(activityName).stream().filter(MonitoredData::isShortActivity)
					.count();
			if ((100 * matches) / totalSize > 90.0f)
				shortActivities.add(activityName.toString());
		}
		return shortActivities;
	}

	public static void writeTask(List<String> shortActivities) {

		File task6 = new File("Task_6.txt");
		try {
			FileWriter fw6 = new FileWriter(task6);
			fw6.write("Activities with more than 90% of the monitored records lasting less than 5 minutes:\n\n");
			shortActivities.stream().forEach(e -> {
				try {
					fw6.write(e + "\n");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			});
			fw6.flush();
			fw6.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
