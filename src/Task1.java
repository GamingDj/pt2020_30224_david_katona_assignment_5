import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task1 {

	public static List<MonitoredData> doTask(String path) {

		List<MonitoredData> activities = null;
		try (Stream<String> stream = Files.lines(Paths.get(path))) {
			activities = stream.map(line -> line.split("		"))
					.map(a -> new MonitoredData(a[0], a[1], a[2].replaceAll("\\s", ""))).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (activities==null) {
			System.out.println("Error reading data from file!");
			System.exit(0);
		}
		return activities;
	}

	public static void writeTask(List<MonitoredData> data) {

		try {
			File task1 = new File("Task_1.txt");
			FileWriter fw1 = new FileWriter(task1);
			for (MonitoredData d : data)
				fw1.write(d.toString());
			fw1.flush();
			fw1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
