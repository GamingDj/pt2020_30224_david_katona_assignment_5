import java.time.Duration;
import java.util.List;
import java.util.Map;

public class MainClass {

	public static void main(String[] args) {

		List<MonitoredData> data = Task1.doTask("Activities.txt");
		List<String> days = Task2.doTask(data);
		Map<String, Integer> activityCount = Task3.doTask(data);
		Map<Integer, Map<String, Integer>> activityByDay = Task4.doTask(data);
		// ! LocalTime nu e alegerea buna, max 23:59:59:999999999 => se foloseste Duration
		Map<String, Duration> duration = Task5.doTask(data);
		List<String> shortActivities = Task6.doTask(data);

		Task1.writeTask(data);
		Task2.writeTask(days);
		Task3.writeTask(activityCount);
		Task4.writeTask(activityByDay);
		Task5.writeTask(duration);
		Task6.writeTask(shortActivities);
	}
}
